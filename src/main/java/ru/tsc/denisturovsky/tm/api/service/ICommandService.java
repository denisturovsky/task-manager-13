package ru.tsc.denisturovsky.tm.api.service;

import ru.tsc.denisturovsky.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
